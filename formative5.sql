SELECT
prod.name as product_name , b.name as brandName , m.name as manufacture
, A.city as city , A.country as country,
v.name as valuta , pr.amount as price ,prod.stock
FROM
Manufacturer m 
JOIN Address A
ON m.addressId =  A.id
JOIN Product prod
ON prod.manufactureId = m.id
JOIN Brand b
ON prod.BrandId = b.id
JOIN Price pr
ON pr.productId = prod.id
JOIN Valuta v
ORDER BY price ASC;






