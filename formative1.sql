create database formativedb;
use formativedb;

CREATE TABLE Address(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
street varchar(50),
city varchar(15),
province varchar(15),
country varchar(15),
zipCode varchar(30)
);

CREATE TABLE Brand(
id INT PRIMARY KEY auto_increment NOT NULL,
name varchar(20)
);

CREATE TABLE Valuta(
id INT PRIMARY KEY auto_increment NOT NULL,
code varchar(25),
name varchar(25)
);

CREATE TABLE Manufacturer(
id INT PRIMARY KEY auto_increment NOT NULL,
name varchar(25),
addressId int,
foreign key (addressId) references Address(id)
);

CREATE TABLE Product(
id INT PRIMARY KEY auto_increment NOT NULL,
artNumber varchar(25),
name varchar(25),
description varchar(25),
manufactureId int,
brandId int,
foreign key (manufactureId) references Manufacturer(id),
foreign key (brandId) references Brand(id),
stock int 
);



CREATE TABLE Price(
id INT PRIMARY KEY auto_increment NOT NULL,
productId int,
foreign key (productId) references Product(id),
valutaId int,
foreign key (valutaId) references Valuta(id),
amount int
);

show tables;

