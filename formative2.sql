#insert into table brand
INSERT INTO BRAND VALUES(0,'Honda');
INSERT INTO BRAND VALUES(0,'Isuzu');
SELECT * FROM BRAND;


#insert into table address
INSERT INTO Address VALUES(0,'Jl Mawar No 20','Bogor','Jawa Barat','Indonesia','16132');
INSERT INTO Address VALUES(0,'Jl Melati No 30','Semarang','Jawa Tengah','Indonesia','16132');
SELECT * FROM Address;

#insert into valuta
INSERT INTO VALUTA VALUES(0,'IDR','Rupiah');
INSERT INTO VALUTA VALUES(0,'USD','US Dollar');
SELECT * FROM VALUTA;

#insert into manufacture
INSERT INTO Manufacturer VALUES(0,'ASTRA',1);
INSERT INTO Manufacturer VALUES(0,'DAIHATSU',2);
SELECT * FROM Manufacturer;

#insert into product
INSERT INTO Product VALUES(0,'30','Kijang','Kijang Bagus',3,2,11);
INSERT INTO Product VALUES(0,'40','Avanza','Avanza Bagus',4,1,9);
INSERT INTO Product VALUES(0,'33','Xenia','Xenia Bagus',4,2,0);
INSERT INTO Product VALUES(0,'44','Tesla','Mantap Bagus',3,1,0);
INSERT INTO Product VALUES(0,'55','Panther','Mantep Bagus',3,1,10);
INSERT INTO Product VALUES(0,'65','VW Kodok','Mantaps Bagus',4,2,5);
INSERT INTO Product VALUES(0,'77','Inova','Manteps Bagus',3,2,13);
INSERT INTO Product VALUES(0,'88','Civic','Mobil Baru',4,1,9);
INSERT INTO Product VALUES(0,'99','Jeep','Waow',4,2,8);
INSERT INTO Product VALUES(0,'90','Vios','Warbyasah',3,1,6);
SELECT * FROM Product;

#insert into product prices
INSERT INTO Price VALUES(0,1,1,5000000);
INSERT INTO Price VALUES(0,1,2,50000);
INSERT INTO Price VALUES(0,1,1,6000000);
INSERT INTO Price VALUES(0,1,2,60000);
INSERT INTO Price VALUES(0,1,1,4000000);
INSERT INTO Price VALUES(0,1,2,40000);
INSERT INTO Price VALUES(0,1,1,3000000);
INSERT INTO Price VALUES(0,1,2,30000);
INSERT INTO Price VALUES(0,1,1,8000000);
INSERT INTO Price VALUES(0,1,2,80000);
INSERT INTO Price VALUES(0,1,1,550000);
INSERT INTO Price VALUES(0,1,2,55000);
INSERT INTO Price VALUES(0,1,1,6600000);
INSERT INTO Price VALUES(0,1,2,66000);
INSERT INTO Price VALUES(0,1,1,4400000);
INSERT INTO Price VALUES(0,1,2,44000);
INSERT INTO Price VALUES(0,1,1,3300000);
INSERT INTO Price VALUES(0,1,2,33000);
INSERT INTO Price VALUES(0,1,1,8800000);
INSERT INTO Price VALUES(0,1,2,88000);
SELECT * FROM Price;






